#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\wglew.h"
#include "Dependencies\glm/glm.hpp"
#include "Dependencies\glm/gtc/matrix_transform.hpp"
#include "Dependencies\glm/gtx/euler_angles.hpp"

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	GLuint CreatePngTexture(char * filePath);
	GLuint CreateBmpTexture(char * filePath);
	   
	void Test();
	void DrawSign();
	void Lecture3();
	void SetTime(float Time) {
		m_TotalTimeLecture3 += Time*10.f;
	}
private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects(); 
	unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight);

	bool m_Initialized = false;
	
	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	GLuint	m_VBORect = 0;
	GLuint	m_SolidRectShader = 0;
	GLuint	m_VBOTest = 0;
	GLuint	m_VBOTestColor = 0;
	GLuint	m_VBOSingle = 0;
	float ParticleSize = 0.01f;
	float SingleParticleVertices[18] =
	{
		-1.f * ParticleSize,		-1.f * ParticleSize, 0.f,
		1.f * ParticleSize,		1.f * ParticleSize, 0.f,
		-1.f * ParticleSize,	1.f * ParticleSize, 0.f,

		-1.f * ParticleSize,	-1.f * ParticleSize, 0.f,
		1.f * ParticleSize,		-1.f * ParticleSize, 0.f,
		1.f * ParticleSize,		1.f * ParticleSize, 0.f
	};

private:
	float m_TotalTimeLecture3 = 0.f;
};

