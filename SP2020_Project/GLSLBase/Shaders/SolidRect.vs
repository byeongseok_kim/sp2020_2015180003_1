#version 450

in vec3 a_Position;
//in vec4 a_Color;

//out vec4 v_Color;

uniform float u_Radian;
uniform float u_Time;
void main()
{
	vec4 newPos = vec4(a_Position, 1.f);

	newPos.x +=cos(u_Radian*3.14/180.f);
	newPos.y +=sin(u_Radian*3.14/180.f);

	//newPos.x +=cos(u_Time);
	//newPos.y +=sin(u_Time);

	gl_Position = newPos;
	//v_Color = a_Color;
}
